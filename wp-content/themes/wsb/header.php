<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wsb
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">
	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/6c10f0500f.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gsap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/rellax/1.0.0/rellax.min.js"></script>

</head>

<body <?php body_class(); ?>>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178300907-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-178300907-1');
	</script>
	
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Pular para o conteúdo', 'wsb' ); ?></a>

	<header class="site-header  <?php if( get_field('header_wt') ): ?>header_wt<?php endif; ?> ">
		<section class="top-header-sec">
			<div class="container">
				<div class="branding">
					<?php
					the_custom_logo();
					?>
				</div>


				<nav id="site-navigation" class="main-navigation desktop">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i></i></button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</nav><!-- #site-navigation -->


				<div class="right desktop">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'lang-menu'
					) );
					?>
		    		<?php if ( have_rows('redes_sociais', 'option') ): ?>
		    			<ul class="redes_sociais">
		                <?php while ( have_rows('redes_sociais', 'option') ) : the_row(); 
		                	$pos = get_sub_field('pos');
							if( $pos && in_array('header', $pos) ) { ?>
		                    <li>
		                        <a href="<?php the_sub_field('link'); ?>" target="_blank">
		                        	<img src="<?php the_sub_field('icone'); ?>">
		                        </a>
		                    </li>
		                	<?php } endwhile; ?>
		    			</ul>
		    		<?php endif; ?>	
				</div>	
			</div>
		</section>
		<div class="menu-mobile">
			<div class="right">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'lang-menu'
				) );
				?>
	    		<?php if ( have_rows('redes_sociais', 'option') ): ?>
	    			<ul class="redes_sociais">
	                <?php while ( have_rows('redes_sociais', 'option') ) : the_row(); 
	                	$pos = get_sub_field('pos');
						if( $pos && in_array('header', $pos) ) { ?>
	                    <li>
	                        <a href="<?php the_sub_field('link'); ?>" target="_blank">
	                        	<img src="<?php the_sub_field('icone'); ?>">
	                        </a>
	                    </li>
	                	<?php } endwhile; ?>
	    			</ul>
	    		<?php endif; ?>	
			</div>	
			<nav class="main-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1'
				) );
				?>
			</nav>
			
		</div>
	</header><!-- #masthead -->
	


	<section id="content" class="miolo-site">
